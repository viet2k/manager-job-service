package com.example.jobmanagerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobManagerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobManagerServiceApplication.class, args);
	}

}
